import Vue from 'vue';
import VueOnsen from 'vue-onsenui';
import App from './App.vue';
import router from './router';
import '../node_modules/onsenui/css/onsenui.css';
import '../node_modules/onsenui/css/onsen-css-components.css';

Vue.use(VueOnsen);
Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
