import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import About from './views/About.vue';
import Timeline from './views/Timeline.vue';
import Notice from './views/Notice.vue';
import Page from './views/Page.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    {
      path: '/Timeline',
      name: 'Timeline',
      component: Timeline,
    },
    {
      path: '/notice',
      name: 'notice',
      component: Notice,
    },
    {
      path: '/page',
      name: 'page',
      component: Page,
    },
  ],
});
